package com.hackerrank.eshopping.product.dashboard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackerrank.eshopping.product.dashboard.model.Product;
import com.hackerrank.eshopping.product.dashboard.persistence.ProductRepository;
import com.hackerrank.eshopping.product.dashboard.request.ProductUpdateRequest;

import javassist.tools.web.BadHttpRequest;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;

	public boolean existsProduct(Long id) {
		return productRepository.existsById(id);
	}
	
	public List<Product> customService (String category, Boolean availability) {
		
		try {
			
			if(category.isEmpty() && availability==null) {
				return productRepository.findAllByOrderByIdAsc();
			}
			
			if(!category.isEmpty()) {
				return productRepository.customQuery1(category);
			}
			
			if(!category.isEmpty() && availability!=null) {
				return productRepository.customQuery2(category, availability);
			}
		
		} catch (Exception e) {
			e.getCause();
			e.getMessage();
		}
		return null;
}
	
	public List<Product> customQuery1(String category){
		return productRepository.customQuery1(category);
	}
	
	public List<Product> customQuery2(String category, Boolean availability){
		return productRepository.customQuery2(category, availability);
	}
	
	public List<Product> getAllProducts(){
		return productRepository.findAllByOrderByIdAsc();
	}
	
	public Product findProductById(Long id) {
		return productRepository.findById(id).get();
	}

	public Product addProduct(Product product) {
	
		return productRepository.save(product);
		
	}
	
	public Product updateProduct(ProductUpdateRequest productUpdateRequest, Long id ) throws BadHttpRequest {
		
		
		Product prod= productRepository.findById(id).get();
		
		//Product product = new Product();
		//id= product.getId();
		prod.setRetailPrice(productUpdateRequest.getRetail_price());
		prod.setDiscountedPrice(productUpdateRequest.getDiscounted_price());
		prod.setAvailability(productUpdateRequest.getAvailability());
		
		productRepository.findById(prod.getId());
		return productRepository.save(prod);
	}

}
