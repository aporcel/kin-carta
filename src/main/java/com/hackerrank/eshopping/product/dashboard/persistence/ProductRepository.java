package com.hackerrank.eshopping.product.dashboard.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hackerrank.eshopping.product.dashboard.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>{
	
	boolean existsById(Long id);
	
	List<Product> findAllByOrderByIdAsc();
	
	public final String SQL_1= "select * from Product where category= ?1 order by (case when availability = 0 then ~0 else availability end), discounted_price asc";
	
	public final String SQL_2= "select * from Product Where category= ?1 AND availability= ?2 order by discounted_price desc, id asc";
	
	@Query(value= SQL_1, nativeQuery=true)
	List<Product> customQuery1(String category);

	@Query(value = SQL_2, nativeQuery = true)
	List<Product> customQuery2(String category, Boolean availability);
	
	
}
