package com.hackerrank.eshopping.product.dashboard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hackerrank.eshopping.product.dashboard.model.Product;
import com.hackerrank.eshopping.product.dashboard.request.ProductUpdateRequest;
import com.hackerrank.eshopping.product.dashboard.service.ProductService;

import javassist.tools.web.BadHttpRequest;

@RestController
@RequestMapping(value = "/products")
public class ProductsController {

	@Autowired
	private ProductService productService;
	
	
	@SuppressWarnings("unused")
	@GetMapping
	public ResponseEntity<List<Product>> getProducts(@RequestParam(value = "category", required = false) String category,
			@RequestParam(value = "availability", required = false) Boolean availability) {
		
		if(category!=null && availability==null) {
			return new ResponseEntity<>(productService.customQuery1(category),HttpStatus.OK);
		}
		if(category!=null && availability!=null) {
			return new ResponseEntity<>(productService.customQuery2(category, availability), HttpStatus.OK);
		}
		
		return new ResponseEntity<>(productService.getAllProducts(), HttpStatus.OK);
		
	}

	@GetMapping("/{product_id}")
	public ResponseEntity<Product> getProductById(@PathVariable Long product_id) {

		if (productService.existsProduct(product_id)) {
			return new ResponseEntity<>(productService.findProductById(product_id), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	

	@PostMapping()
	public ResponseEntity<Product> addProduct(@RequestBody Product product) {

		if (!productService.existsProduct(product.getId())) {
			return new ResponseEntity<>(productService.addProduct(product), HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<Product> updateProduct(@PathVariable Long id,
			@RequestBody ProductUpdateRequest productUpdateRequest) throws BadHttpRequest {

		if (productService.existsProduct(id)) {
			return new ResponseEntity<>(productService.updateProduct(productUpdateRequest, id), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}
